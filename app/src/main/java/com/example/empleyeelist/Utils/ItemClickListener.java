package com.example.empleyeelist.Utils;

public interface ItemClickListener<TYPE> {
    public void onItemClick(TYPE item, int position);
}
