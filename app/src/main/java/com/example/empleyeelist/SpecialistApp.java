package com.example.empleyeelist;

import android.app.Application;

import com.example.empleyeelist.DateBase.DBService;
import com.example.empleyeelist.Network.ApiInterface;
import com.example.empleyeelist.Network.RetrofitClient;

public class SpecialistApp extends Application {
    private static SpecialistApp instance;
    public DBService appDb;

    public static SpecialistApp Instance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appDb = DBService.getDataBase(instance);
    }

    public ApiInterface getRestService() {
        return RetrofitClient.restService();
    }
}
