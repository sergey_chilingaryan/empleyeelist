package com.example.empleyeelist.Network;

import com.example.empleyeelist.Network.Models.ResponseFromGitLab;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("65gb/static/raw/master/testTask.json")
    Call<ResponseFromGitLab> getFeed();
}
