package com.example.empleyeelist.DateBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.example.empleyeelist.DateBase.Converters.SpecialistsConverter;
import com.example.empleyeelist.DateBase.Dao.SpecialistDao;
import com.example.empleyeelist.DateBase.Entity.SpecialistEntity;

@Database(entities = {SpecialistEntity.class}, version = 1, exportSchema = false)
@TypeConverters({SpecialistsConverter.class})
public abstract class DBService extends RoomDatabase {
    private static DBService INSTANCE = null;

    public static DBService getDataBase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder
                    (context, DBService.class, "specialists.db")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public abstract SpecialistDao specialistDao();

}
